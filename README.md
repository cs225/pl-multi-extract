Grab MultiFile-type question data out of a PrairieLearn CSV.

Usage:

```
pl-multi-extract -csv=/tmp/exam_final_submissions.csv -output='/tmp/files/$netid/' -qid='exam-qid.*'
```
